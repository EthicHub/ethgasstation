# ethgasstation
#### an adaptive gas price oracle for the ethereum blockchain

This is the backend for [ethgasstation](https://ethgasstation.info), written in
Python 3. This python script is designed to monitor a local Geth or Parity node. It will
record data about pending and mined transactions, including the transactions in
your node's transaction pool. Its main purpose is to generate adaptive gas price
estimates that enable you to know what gas price to use depending on your
confirmation time needs. It generates these estimates based on miner policy
estimates as well as the number of transactions in the txpool and the gas
offered by the transaction.

The basic strategy is to use statistical modelling to predict confirmation times
at all gas prices from 0-100 gwei at the current state of the txpool and minimum
gas prices accepted in blocks over the last 200 blocks.  Then, it selects the
gas price that gives the desired confirmation time assuming standard gas offered
(higher than 1m gas is slower).

### Installation and Prerequisites

It is also possible to run the oracle, redis and MariaDB as a **Docker compose**.
Read ethgasstation-api/README.md and ethgasstation-backend/README.md

1. Change the settings in *ethgastation-backend/settings.docker.conf*, *ethgasstation-api/settings.conf* and *ethgasstation-api/settings.docker.conf*.
2. Run `docker-compose -f compose.yml build` from this directory.
3. Run `docker-compose -f compose up`.
